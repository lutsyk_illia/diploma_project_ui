import { Task } from './task';
import { User } from './user';

export class Discipline{
    DisciplineID: number;
    DisciplineName: string;
    Description: string;
    Tasks: Task[];
    Users: User[];
}