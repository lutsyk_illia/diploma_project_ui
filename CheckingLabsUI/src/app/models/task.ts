import { TestCase } from './testcases';

export class Task{
    TaskID: number;
    TaskDescription: string;
    TaskInputExample: string;
    TaskOutputExample: string;
    DateCreated: Date;
    TestCases: TestCase[];
}