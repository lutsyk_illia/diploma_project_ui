export class TestCase{
    TestCaseID: number;
    TestInputData: string;
    TestOutputExpected: string;
    Difficulty: number;
}