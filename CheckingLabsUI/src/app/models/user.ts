import { Role } from './role';
import { Group } from './group';

export class User{
    UserID: number;
    Username: string;
    Password: string;
    Firstname: string;
    Lastname: string;
    MiddleName: string;
    IsActive: boolean;
    Email: string;
    RoleID: number;
    Role: Role;
    Group: Group;
}