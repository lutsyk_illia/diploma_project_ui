import { User } from './user';
import { Task } from './task';

export class UserTask{
    UserID: number;
    TaskID: number;
    UploadedFile: string;
    DateUploaded: Date;
    Rating: number;

    /*user: User;
    task: Task;*/
}