import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { TestCase } from '../models/testcases';

@Injectable()
export class TestCasesService {
    constructor(private http: HttpClient, private config: AppConfig) {}

    getAllTestCases(){
        return this.http.get<TestCase[]>(this.config.apiUrl + 'api/testcases');
    }

    getTestCaseInfo(id: number){
        return this.http.get<TestCase>(this.config.apiUrl + 'api/testcases/' + id);
    }

    updateTestCaseInfo(id: number, testCase: TestCase){
        return this.http.put(this.config.apiUrl + '/api/testcases/' + id, testCase);
    }

    registerTestCase(testCase: TestCase){
        return this.http.post(this.config.apiUrl + 'api/testcases', testCase);
    }

    deleteTestCase(id: number){
        return this.http.delete(this.config.apiUrl + 'api/testcases/' + id);
    }
    
}