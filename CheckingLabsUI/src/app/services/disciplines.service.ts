import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Discipline } from '../models/discipline';
import { map } from 'rxjs/operators';
import { Task } from '../models/task';

@Injectable()
export class DisciplineService {
    constructor(private http: HttpClient, private config: AppConfig) {}

    getAllDisciplines() {
        return this.http.get<Discipline[]>(this.config.apiUrl + 'api/disciplines');
    }
    
    getDisciplineInfo(id: number) {
        return this.http.get<Discipline>(this.config.apiUrl + '/api/disciplines/' + id);
    }

    updateDisciplineInfo(id: number, discipline: Discipline){
        return this.http.put(this.config.apiUrl + '/api/disciplines/' + id, discipline);
    }

    registerDiscipline(discipline: Discipline){
        return this.http.post(this.config.apiUrl + '/api/disciplines', discipline);
    }

    insertTask(id: number, task: Task){
        return this.http.post(this.config.apiUrl+'/api/disciplines/'+id+"/insertTask", task)
    }

    deleteDiscipline(id: number) {
        return this.http.delete(this.config.apiUrl + '/api/disciplines/' + id);
    }
}