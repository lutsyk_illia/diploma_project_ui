import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Discipline } from '../models/discipline';
import { map } from 'rxjs/operators';
import { Task } from '../models/task';
import { TestCase } from '../models/testcases';

@Injectable()
export class TaskService {
    constructor(private http: HttpClient, private config: AppConfig) {}

    getAllTasks() {
        return this.http.get<Task[]>(this.config.apiUrl + 'api/tasks');
    }
    
    getTaskInfo(id: number) {
        return this.http.get<Task>(this.config.apiUrl + '/api/tasks/' + id);
    }

    updateTaskInfo(id: number, task: Task){
        return this.http.put(this.config.apiUrl + '/api/tasks/' + id, task);
    }

    registerTask(task: Task){
        return this.http.post<Task>(this.config.apiUrl + '/api/tasks', task);
    }

    addTestCase(id: number, testCase: TestCase){
        return this.http.post(this.config.apiUrl + '/api/tasks/'+id+'/addTestCase', testCase);
    }

    deleteTask(id: number) {
        return this.http.delete(this.config.apiUrl + '/api/tasks/' + id);
    }
}