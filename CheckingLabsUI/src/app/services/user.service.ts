import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map, catchError} from 'rxjs/operators';
import { AppConfig } from '../app.config';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Discipline } from '../models/discipline';

@Injectable()
export class UserService {
    constructor(private http: HttpClient, private config: AppConfig) {}

    getAllUsers() {
        return this.http.get<User[]>(this.config.apiUrl + 'api/users');
    }

    getSubscriptions(id: number){
        return this.http.get<Discipline[]>(this.config.apiUrl+'api/user/'+id+'/getsubscriptions');
    }
    
    getUserInfo(id: number) {
        return this.http.get<User>(this.config.apiUrl + '/api/users/' + id);
    }

    updateUserInfo(id: number, user: User){
        return this.http.put(this.config.apiUrl + '/api/users/' + id, user);
    }

    registerUser(user: User){
        return this.http.post(this.config.apiUrl + '/api/users', user);
    }

    deleteUser(id: number) {
        return this.http.delete(this.config.apiUrl + '/api/users/' + id);
    }
}