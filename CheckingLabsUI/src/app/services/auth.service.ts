import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { map } from 'rxjs/operators';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  private isLoggedin = false;
  private options: HttpHeaders;
  
  constructor(private router: Router,
    private http: HttpClient, private config: AppConfig) { 
    this.options = new HttpHeaders({ 'Content-Type': 'application/json'} );
  }

  static token() {
    const currentUser = localStorage.getItem('currentUserToken');
    if (currentUser) {
      return {
        headers: new HttpHeaders({'Authorization': 'Bearer ' + currentUser})
      };
    }
  }

  login(body) {

    console.log(body);
    return this.http.post(this.config.apiUrl + 'token', body,
      {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')})
      // .pipe(
      //   map((response: Response) => {
      //      console.log(response.json().toString().split('&'));
      //     const responseStrings = response.json().toString().split('&');
      //     if (responseStrings) {
      //        localStorage.setItem('currentUserToken', responseStrings[0]);
      //        localStorage.setItem('currentUserName', responseStrings[1] + ' ' + responseStrings[2]);
      //        localStorage.setItem('currentUserId', responseStrings[3]);
      //      }
      //   })
      // );
  }

  logout() {
    this.http.post(this.config.apiUrl + '/api/account/logout', '', AuthService.token());
    localStorage.removeItem('currentUserToken');
    localStorage.removeItem('currentUserName');
    localStorage.removeItem('currentUserId');
    localStorage.removeItem('currentUserRole');
    this.router.navigate(['/home']);
  }

  isTeacherOrAdmin(){
    if(localStorage.getItem('currentUserRole') == "teacher" || localStorage.getItem('currentUserRole') == "admin"){
      return true;
    }
    else {
      return false;
    }
  }

  isLoggedIn() {
    if (localStorage.getItem('currentUserToken') == null) {
      this.isLoggedin = false;
      return this.isLoggedin;
    } else {
      return true;
    }
  }
}
