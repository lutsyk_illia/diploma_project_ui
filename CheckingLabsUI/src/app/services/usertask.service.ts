import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { AppConfig } from '../app.config';
import { Discipline } from '../models/discipline';
import { map } from 'rxjs/operators';
import { Task } from '../models/task';
import { UserTask } from '../models/usertasks';

@Injectable()
export class UserTaskService {
    constructor(private http: HttpClient, private config: AppConfig) {}

    getAllUserTask() {
        return this.http.get<UserTask[]>(this.config.apiUrl + 'api/UserTasks');
    }
    
    getUserTaskInfo(id: number) {
        return this.http.get<UserTask[]>(this.config.apiUrl + '/api/UserTasks/' + id + '/results');
    }

    getUserTaskInfoByTask(id: number) {
        return this.http.get<UserTask[]>(this.config.apiUrl + '/api/UserTasks/resultsByTask/'+id);
    }
}