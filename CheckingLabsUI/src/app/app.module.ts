import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule, routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/directives/navbar/navbar.component';
import { FooterComponent } from './components/directives//footer/footer.component';
import { AlertComponent } from './components/directives/alert/alert.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AuthGuard } from './guards/auth.guard';
import { AlertService } from './services/alert.service';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './components/login/login.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'; 
import { UserService } from './services/user.service';
import { AppConfig } from './app.config';
import { RegistrationComponent } from './components/registration/registration.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { DisciplineCardsItemComponent } from './components/discipline-cards-item/discipline-cards-item.component';
import { CreateDisciplineComponent } from './components/create-discipline/create-discipline.component';
import { DisciplineCardsComponent } from './components/discipline-cards/discipline-cards.component';
import { DisciplineService } from './services/disciplines.service';
import { ViewDisciplineComponent } from './components/view-discipline/view-discipline.component';
import { TaskCardItemComponent } from './components/task-card-item/task-card-item.component';
import { TaskService } from './services/task.service';
import { TaskDetailsComponent } from './components/task-details/task-details.component';
import { ViewResultComponent } from './components/view-result/view-result.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import { UserTaskService } from './services/usertask.service';
import { TestCasesService } from './services/testcases.service';
import { TestcasesCardsComponent } from './components/testcases-cards/testcases-cards.component';
import { CreateTestcaseComponent } from './components/create-testcase/create-testcase.component';
import { TestcaseDetailComponent } from './components/testcase-detail/testcase-detail.component';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    AlertComponent,
    LoginComponent,
    RegistrationComponent,
    CreateTaskComponent,
    DisciplineCardsItemComponent,
    CreateDisciplineComponent,
    DisciplineCardsComponent,
    ViewDisciplineComponent,
    TaskCardItemComponent,
    TaskDetailsComponent,
    ViewResultComponent,
    TestcasesCardsComponent,
    CreateTestcaseComponent,
    TestcaseDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BsDropdownModule,
    FormsModule,
    MatSelectModule,
    HttpClientModule,
    NgbModule,
    MatTableModule,
    NgbModule.forRoot(),
    routing
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthService,
    AppConfig,
    UserService,
    DisciplineService,
    TaskService,
    UserTaskService,
    TestCasesService
  ],
  entryComponents: [LoginComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
