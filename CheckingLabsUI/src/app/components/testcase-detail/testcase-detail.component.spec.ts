import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcaseDetailComponent } from './testcase-detail.component';

describe('TestcaseDetailComponent', () => {
  let component: TestcaseDetailComponent;
  let fixture: ComponentFixture<TestcaseDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestcaseDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestcaseDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
