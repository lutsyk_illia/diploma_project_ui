import { Component, OnInit, Input } from '@angular/core';
import { TestCase } from 'src/app/models/testcases';
import { ActivatedRoute } from '@angular/router';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { TestCasesService } from 'src/app/services/testcases.service';

@Component({
  selector: 'app-testcase-detail',
  templateUrl: './testcase-detail.component.html',
  styleUrls: ['./testcase-detail.component.css']
})
export class TestcaseDetailComponent {

  @Input() testCase: TestCase;

  constructor(private route: ActivatedRoute, private testCasesService: TestCasesService) { }

  deleteTestCase(){
    this.testCasesService.deleteTestCase(this.testCase.TestCaseID).subscribe(
      response => window.location.reload(),
      error => console.log(error)
    )
  }

}
