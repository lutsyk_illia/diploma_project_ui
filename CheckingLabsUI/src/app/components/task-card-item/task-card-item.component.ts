import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { Router } from '@angular/router';
import { AppConfig } from 'src/app/app.config';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-task-card-item',
  templateUrl: './task-card-item.component.html',
  styleUrls: ['./task-card-item.component.css']
})
export class TaskCardItemComponent {

  @Input() task: Task;
  file: File

  constructor(public taskService: TaskService, private router: Router, private http: HttpClient,
    private appConfig: AppConfig, public authService: AuthService) { }

  fileChange(file) {
    this.file = file.target.files[0];
    console.log(this.file.name);
    this.uploadFile();
  }

  uploadFile(){
    let formData = new FormData();
    formData.append('file', this.file, this.file.name);
    // TODO : change user and task id
    var userId = localStorage.getItem("currentUserId");
    this.http.post(this.appConfig.apiUrl+"api/UserTasks/User/"+userId+"/Task/"+this.task.TaskID+"/uplfile", formData)
    .subscribe((val) => {
      console.log(val);
      });
      return false; 
  }

  deleteTask(){
    this.taskService.deleteTask(this.task.TaskID).subscribe(
      response => {console.log(response); window.location.reload();},
      error => console.log(error)
    );
  }

}
