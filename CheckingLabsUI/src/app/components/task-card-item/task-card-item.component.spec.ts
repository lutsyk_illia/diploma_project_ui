import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskCardItemComponent } from './task-card-item.component';

describe('TaskCardItemComponent', () => {
  let component: TaskCardItemComponent;
  let fixture: ComponentFixture<TaskCardItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskCardItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskCardItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
