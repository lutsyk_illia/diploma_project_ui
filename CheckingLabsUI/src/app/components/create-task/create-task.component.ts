import { Component, OnInit } from '@angular/core';
import { Discipline } from 'src/app/models/discipline';
import { Task } from 'src/app/models/task';
import { ActivatedRoute, Router } from '@angular/router';
import { DisciplineService } from 'src/app/services/disciplines.service';
import { TaskService } from 'src/app/services/task.service';
import { resolveComponentResources } from '@angular/core/src/metadata/resource_loading';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {

  id: number
  discipline: Discipline = new Discipline()
  task: Task = new Task()
  
  constructor(private route: ActivatedRoute, public disciplineService: DisciplineService, 
    public taskService: TaskService, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['dId']
      this.disciplineService.getDisciplineInfo(this.id).subscribe(discipline =>{
        this.discipline = discipline
        console.log(this.discipline);
      });
    });
    console.log(this.id);
    console.log(this.discipline);
  }

  createTask(){
    this.disciplineService.insertTask(this.id, this.task).subscribe(
      response => {console.log(response); this.router.navigate(['disciplineDetail',this.id])},
      error => console.log(error)
      
    )
  }

}
