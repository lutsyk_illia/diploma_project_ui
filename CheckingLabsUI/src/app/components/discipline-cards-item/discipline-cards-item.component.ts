import { Component, OnInit, Input } from '@angular/core';
import { Discipline } from 'src/app/models/discipline';
import { DisciplineService } from 'src/app/services/disciplines.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-discipline-cards-item',
  templateUrl: './discipline-cards-item.component.html',
  styleUrls: ['./discipline-cards-item.component.css']
})
export class DisciplineCardsItemComponent{
  @Input() discipline: Discipline;
  
  constructor(public disciplineService: DisciplineService, private router: Router, public authService: AuthService) {
   }

  openCoursePage() {
    //this.router.navigate(['courses/', this.discipline.disciplineID]);
  }

  deleteDiscipline(){
    this.disciplineService.deleteDiscipline(this.discipline.DisciplineID).subscribe(
      response => {console.log(response); window.location.reload();},
      error => console.log(error)
    )
  }

}
