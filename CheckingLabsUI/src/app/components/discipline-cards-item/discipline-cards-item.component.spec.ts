import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisciplineCardsItemComponent } from './discipline-cards-item.component';

describe('DisciplineCardsItemComponent', () => {
  let component: DisciplineCardsItemComponent;
  let fixture: ComponentFixture<DisciplineCardsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisciplineCardsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisciplineCardsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
