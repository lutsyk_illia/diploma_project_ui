import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisciplineCardsComponent } from './discipline-cards.component';

describe('DisciplineCardsComponent', () => {
  let component: DisciplineCardsComponent;
  let fixture: ComponentFixture<DisciplineCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisciplineCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisciplineCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
