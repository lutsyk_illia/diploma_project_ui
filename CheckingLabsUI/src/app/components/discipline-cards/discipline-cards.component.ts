import { Component, OnInit } from '@angular/core';
import { DisciplineService } from 'src/app/services/disciplines.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Discipline } from 'src/app/models/discipline';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-discipline-cards',
  templateUrl: './discipline-cards.component.html',
  styleUrls: ['./discipline-cards.component.css']
})
export class DisciplineCardsComponent implements OnInit {

  disciplineList : Discipline[] = [];
  currentCategory = 'All courses';

  constructor(public disciplineService: DisciplineService, private activatedRoute: ActivatedRoute, 
    private router: Router, private userService: UserService) { }
  
  ngOnInit() : void {
    // const paraMap = this.activatedRoute.snapshot.queryParamMap;
    // this.currentCategory = paraMap.get('category');
    // this.currentCategory = 'All courses';
    console.log(this.currentCategory);
    this.disciplineList = []

    this.activatedRoute.params.subscribe(params => {
      this.currentCategory = params['courses']
    });

    if(this.currentCategory != "my"){
      this.currentCategory = 'All courses';
      this.disciplineService.getAllDisciplines().subscribe(data => {
        data.forEach(element => {
        this.disciplineList.push(element)
      })});
      console.log(this.disciplineList);
    }
    else{
      this.currentCategory = 'My courses';
      var userId = parseInt(localStorage.getItem("currentUserId"));
      this.userService.getSubscriptions(userId).subscribe(data => {
        data.forEach(element => {
          this.disciplineList.push(element)
        })
      })
    }
    
  }

}
