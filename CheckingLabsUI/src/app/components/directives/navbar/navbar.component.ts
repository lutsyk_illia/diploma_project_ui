import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  moduleId: module.id,
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  users: User[];

  constructor(private authService: AuthService, private router: Router, private userService: UserService) { }

  ngOnInit() {
    //this.userService.getAllUsers().subscribe(data => this.users = data);
  }

  Logout() {
    this.authService.logout();
  }

  getCurrentUserName(): string {
    return localStorage.getItem('currentUserName');
  }
  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

}
