import { Component, OnInit } from '@angular/core';
import { TestCase } from 'src/app/models/testcases';
import { Task } from 'src/app/models/task';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from 'src/app/app.config';
import { TaskService } from 'src/app/services/task.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-testcases-cards',
  templateUrl: './testcases-cards.component.html',
  styleUrls: ['./testcases-cards.component.css']
})
export class TestcasesCardsComponent implements OnInit {

  id: number
  task: Task = new Task()

  constructor(private route: ActivatedRoute, private taskService: TaskService, private http: HttpClient,
    private appConfig: AppConfig) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['tId']
      this.taskService.getTaskInfo(this.id).subscribe(task =>{
        this.task = task
        console.log(this.task);
      });
    });
    console.log(this.id);
    console.log(this.task);
  }

}
