import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcasesCardsComponent } from './testcases-cards.component';

describe('TestcasesCardsComponent', () => {
  let component: TestcasesCardsComponent;
  let fixture: ComponentFixture<TestcasesCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestcasesCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestcasesCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
