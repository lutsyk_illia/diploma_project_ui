import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { ActivatedRoute } from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { AppConfig } from 'src/app/app.config';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {
  id: number
  task: Task = new Task()
  file: File

  constructor(private route: ActivatedRoute, private taskService: TaskService, private http: HttpClient,
    private appConfig: AppConfig, public authService: AuthService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['tId']
      this.taskService.getTaskInfo(this.id).subscribe(task =>{
        this.task = task
        console.log(this.task);
      });
    });
    console.log(this.id);
    console.log(this.task);
  }

  fileChange(file) {
    this.file = file.target.files[0];
    console.log(this.file.name);
    this.uploadFile();
  }

  uploadFile(){
    let formData = new FormData();
    formData.append('file', this.file, this.file.name);
    // TODO : change user and task id
    var userId = localStorage.getItem("currentUserId");
    this.http.post(this.appConfig.apiUrl+"api/UserTasks/User/"+userId+"/Task/"+this.id+"/uplfile", formData)
    .subscribe((val) => {
      console.log(val);
      },
      error => alert(error)
      );
      return false; 
  }

}
