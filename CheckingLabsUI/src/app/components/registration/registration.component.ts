import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  model : any = {}
  loading = false;
  returnUrl: string;
  isSuccess: boolean;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService) { }

  ngOnInit() {
  }

  register(){
    var user = new User();
    user.Email = this.model.email;
    user.Firstname = this.model.firstname;
    user.Lastname = this.model.lastname;
    user.Username = this.model.username;
    user.Password = this.model.password;
    if(this.model.teacher == true){
      user.RoleID = 2;
    } else {
      user.RoleID = 3;
    }
    this.userService.registerUser(user).subscribe(
      response => {console.log(response); this.router.navigate(['login'])},
      error => console.log(error)
    )
  }
}
