import { Component, OnInit } from '@angular/core';
import { Discipline } from 'src/app/models/discipline';
import { DisciplineService } from 'src/app/services/disciplines.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig } from 'src/app/app.config';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-create-discipline',
  templateUrl: './create-discipline.component.html',
  styleUrls: ['./create-discipline.component.css']
})
export class CreateDisciplineComponent implements OnInit {

  discipline: Discipline = new Discipline()

  constructor(public disciplineService: DisciplineService, public alertService: AlertService, private router: Router, private config: AppConfig,
    private _route: ActivatedRoute) { }

  ngOnInit() {
  }

  createCourse() {
    console.log(this.discipline);
    this.disciplineService.registerDiscipline(this.discipline).subscribe(
      response => {console.log(response); this.router.navigate(['disciplines','all'])},
      error => {this.alertService.error(error._body); console.log(error);}
      
    )
  }

}
