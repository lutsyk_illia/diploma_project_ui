import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TestCase } from 'src/app/models/testcases';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig } from 'src/app/app.config';
import { TaskService } from 'src/app/services/task.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-testcase',
  templateUrl: './create-testcase.component.html',
  styleUrls: ['./create-testcase.component.css']
})
export class CreateTestcaseComponent implements OnInit {

  id: number
  task: Task = new Task()
  testCase: TestCase = new TestCase()

  constructor(private route: ActivatedRoute, private taskService: TaskService, private http: HttpClient,
    private appConfig: AppConfig, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['tId']
      this.taskService.getTaskInfo(this.id).subscribe(task =>{
        this.task = task
        console.log(this.task);
      });
    });
    console.log(this.id);
    console.log(this.task);
  }

  createTestCase(){
    console.log(this.testCase);
    this.taskService.addTestCase(this.id, this.testCase).subscribe(
      response => {console.log(response); this.router.navigate(['viewTestCases',this.id])},
      error => console.log(error)
    )
  }

}
