import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DisciplineService } from 'src/app/services/disciplines.service';
import { Discipline } from 'src/app/models/discipline';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-view-discipline',
  templateUrl: './view-discipline.component.html',
  styleUrls: ['./view-discipline.component.css']
})
export class ViewDisciplineComponent implements OnInit {
  id: number
  discipline: Discipline = new Discipline()

  constructor(private route: ActivatedRoute, private disciplineService: DisciplineService, public authService: AuthService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['dId']
      this.disciplineService.getDisciplineInfo(this.id).subscribe(discipline =>{
        this.discipline = discipline
        console.log(this.discipline);
      });
    });
    console.log(this.id);
    console.log(this.discipline);
  }

}
