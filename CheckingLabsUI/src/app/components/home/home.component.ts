import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { DisciplineService } from 'src/app/services/disciplines.service';
import { Discipline } from 'src/app/models/discipline';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private route: ActivatedRoute, public userService: UserService, public taskService: TaskService) { }

  ngOnInit() {
    localStorage.removeItem('Users');
    localStorage.removeItem('Tasks');
    this.userService.getAllUsers().subscribe(
      data => localStorage.setItem('Users', JSON.stringify(data))
    )
    this.taskService.getAllTasks().subscribe(
      data => localStorage.setItem('Tasks', JSON.stringify(data))
    )
  }

}
