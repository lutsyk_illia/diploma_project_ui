import { Component, OnInit } from '@angular/core';
import { UserTask } from 'src/app/models/usertasks';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from 'src/app/app.config';
import { TaskService } from 'src/app/services/task.service';
import { HttpClient } from '@angular/common/http';
import { UserTaskService } from 'src/app/services/usertask.service';
import { AuthService } from 'src/app/services/auth.service';
import { Discipline } from 'src/app/models/discipline';
import { User } from 'src/app/models/user';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-view-result',
  templateUrl: './view-result.component.html',
  styleUrls: ['./view-result.component.css']
})
export class ViewResultComponent implements OnInit {

  selectedUser: number = null;
  selectedTask: number = null;
  username = "";
  displayedColumns: string[] = ['Username', 'TaskID', 'DateUploaded', 'Rating'];
  dataSource = [];
  Users: User[] = [];
  Tasks: Task[];
  constructor(private route: ActivatedRoute, private userTaskService: UserTaskService, private http: HttpClient,
    private appConfig: AppConfig, public authService: AuthService) { }

  getData(){
    console.log(this.selectedTask)
    this.Tasks = JSON.parse(localStorage.getItem('Tasks'));
    this.Users = JSON.parse(localStorage.getItem('Users'));
    this.username = localStorage.getItem('currentUserName')
    this.dataSource = [];
    var currentId = parseInt(localStorage.getItem("currentUserId"));
    if(localStorage.getItem("currentUserRole") == "student"){
    this.userTaskService.getUserTaskInfo(currentId).subscribe(
      data => {this.dataSource = data;
      for (let index = 0; index < this.dataSource.length; index++) {
        this.dataSource[index].UserID = this.username;
      }
      }
    )
  }
  else{
    this.userTaskService.getAllUserTask().subscribe(
      data => {this.dataSource = data;
        for (let index = 0; index < this.dataSource.length; index++) {
          this.dataSource[index].UserID = this.Users.find(us => us.UserID == this.dataSource[index].UserID).Username
        }
      }
    )
  }
  }

  getFilterData(){
    console.log(this.selectedUser)
    if(this.selectedTask != null && this.selectedUser != null){

    }
    else if(this.selectedUser != null){
      this.userTaskService.getUserTaskInfo(this.selectedUser).subscribe(
        data => {this.dataSource = data;
          for (let index = 0; index < this.dataSource.length; index++) {
            this.dataSource[index].UserID = this.Users.find(us => us.UserID == this.dataSource[index].UserID).Username
          }
        }
      )
    }
    else{
      this.userTaskService.getUserTaskInfoByTask(this.selectedTask).subscribe(
        data => {this.dataSource = data;
          for (let index = 0; index < this.dataSource.length; index++) {
            this.dataSource[index].UserID = this.Users.find(us => us.UserID == this.dataSource[index].UserID).Username
          }
        }
      )
    }
  }

  Clear(){
    this.selectedTask = null;
    this.selectedUser = null;
  }
  ngOnInit() {
    this.getData();
  }

  getTotal(){
    return Math.round(this.dataSource.map(t => t.Rating).reduce((acc, value) => acc + value, 0) * 100 / this.dataSource.length) / 100;
  }

}
