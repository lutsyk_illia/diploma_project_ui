import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
declare let $: any;

@Component({
  selector: 'app-login',
  moduleId: module.id,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model : any = {}
  loading = false;
  returnUrl: string;
  isSuccess: boolean;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.isSuccess = true;
  }

  login() {
    this.loading = true;
    const body = 'username='+this.model.email+'&password='+this.model.password+'&grant_type=password';
    // body.set('username', this.model.email);
    // body.set('password', this.model.password);
    // body.set('grant_type', 'password');
    // console.log(body.toString());
    this.authenticationService.login(body)
      .subscribe(
        (data : any) => {
          this.isSuccess = true;
          this.router.navigate([this.returnUrl]);
          localStorage.setItem('currentUserToken',data.access_token);
          localStorage.setItem('currentUserName',data.username);
          localStorage.setItem('currentUserId',data.id);
          localStorage.setItem('currentUserRole',data.role);
          console.log(data);
          console.log(localStorage.getItem('currentUserToken'));
          console.log(localStorage.getItem('currentUserId'));
          // $('#login-modal').modal('hide');
        },
        error => {
          this.alertService.error(error._body, false);
          this.loading = false;
          this.isSuccess = false;
        });
  }
}
