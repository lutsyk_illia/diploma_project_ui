import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { DisciplineCardsComponent } from './components/discipline-cards/discipline-cards.component';
import { ViewDisciplineComponent } from './components/view-discipline/view-discipline.component';
import { TaskDetailsComponent } from './components/task-details/task-details.component';
import { CreateDisciplineComponent } from './components/create-discipline/create-discipline.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { ViewResultComponent } from './components/view-result/view-result.component';
import { CreateTestcaseComponent } from './components/create-testcase/create-testcase.component';
import { TestcasesCardsComponent } from './components/testcases-cards/testcases-cards.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'disciplines', component: DisciplineCardsComponent },
  { path: 'disciplineDetail/:dId', component: ViewDisciplineComponent },
  { path: 'taskDetail/:tId', component: TaskDetailsComponent },
  { path: 'createDiscipline', component: CreateDisciplineComponent },
  { path: 'createTask/:dId', component: CreateTaskComponent },
  { path: 'disciplines/:courses', component: DisciplineCardsComponent },
  { path: 'results', component: ViewResultComponent },
  { path: 'createTestCase/:tId', component: CreateTestcaseComponent },
  { path: 'viewTestCases/:tId', component: TestcasesCardsComponent },
  
  { path: '**', redirectTo: 'home' }
];
export const routing = RouterModule.forRoot(routes);
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
